API = "https://api.zarums.tk"
WSK = "wss://wsk.zarums.tk/"

/*
src: https://gist.github.com/jed/982883
*/
function uuid4(a) {
    return (
        a ? (
            a ^ Math.random() * 16 >> a / 4
        ).toString(16) : (
            [1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, uuid4)
    );
}

window.sodium = { onload: function(sodium) {} };

window.addEventListener("resize", async function() {
    ml = $("#msg_list");
    ml.scrollTop(ml[0].scrollHeight);
}, false);

window.addEventListener("load", function() {
    if (typeof WebAssembly != "object") {
        $("#modal__wasm").modal("show");
    }

    feather.replace();

    $("#si_u").keyup(function(e) {
        var code = e.which;
        if (code == 13) e.preventDefault();
        if (code == 32 || code == 13 || code == 188 || code == 186) {
            $("#si_p")[0].focus();
        }
    });

    $("#si_p").keyup(function(e) {
        var code = e.which;
        if (code == 13) e.preventDefault();
        if (code == 32 || code == 13 || code == 188 || code == 186) {
            $("#si_b")[0].click();
        }
    });

    $("#msg").keyup(function(e) {
        var code = e.which;
        if (code == 13) {
            e.preventDefault();
            $("#button-send")[0].click();
        }
    });

    ["#si_u", "#si_p", "#si_b"].forEach(function(a) {
        $(a)[0].disabled = false;
    });
});

/*
    src: https://stackoverflow.com/a/33703102
*/
function concatTypedArrays(a, b) { // a, b TypedArray of same type
    var c = new(a.constructor)(a.length + b.length);
    c.set(a, 0);
    c.set(b, a.length);
    return c;
}


ops = 1
mem = 8388608

function sc_pwhash(password, salt) {
    return sodium.crypto_pwhash(
        sodium.crypto_secretbox_KEYBYTES,
        password,
        salt,
        ops,
        mem,
        sodium.crypto_pwhash_ALG_ARGON2ID13
    );
}

function sc_encrypt(password, message, chat = true) {
    var salt = sodium.randombytes_buf(
        sodium.crypto_pwhash_SALTBYTES
    );
    var nonce = sodium.randombytes_buf(
        sodium.crypto_secretbox_NONCEBYTES
    );

    var k = password;
    if (!chat) {
        k = sc_pwhash(password, salt);
    }

    var rt = sodium.crypto_secretbox_easy(message, nonce, k);
    return concatTypedArrays(concatTypedArrays(salt, nonce), rt);
}

function sc_decrypt(password, ciphertext, chat = true) {
    var salt = ciphertext.slice(
        0,
        sodium.crypto_pwhash_SALTBYTES
    );
    var nonce = ciphertext.slice(
        sodium.crypto_pwhash_SALTBYTES,
        sodium.crypto_pwhash_SALTBYTES + sodium.crypto_secretbox_NONCEBYTES
    );
    var message = ciphertext.slice(
        sodium.crypto_pwhash_SALTBYTES + sodium.crypto_secretbox_NONCEBYTES
    );

    var k = password;
    if (!chat) {
        k = sc_pwhash(password, salt);
    }

    return sodium.crypto_secretbox_open_easy(message, nonce, k);
}

function pc_genpair() {
    return sodium.crypto_box_keypair();
}

function pc_encrypt(s_pk, s_msg) {
    return sodium.crypto_box_seal(s_msg, s_pk);
}

function pc_decrypt(s_pk, s_sk, s_msg) {
    return sodium.crypto_box_seal_open(s_msg, s_pk, s_sk);
}

function encode_base64(input) {
    return sodium.to_base64(input, sodium.base64_variants.ORIGINAL);
}

function decode_base64(b64) {
    return sodium.from_base64(b64, sodium.base64_variants.ORIGINAL);
}

let thisUi = {
    "opened__room_id": null,
    "opened__room_first_message": null,
}
let thisServer = {
    "pk": null,
};
let thisUser = {
    "user_id": 0,
    "username": "",
    "sk": null,
    "pk": null,
    "token": null,
};
let socket;
let rooms_html = {};

function lc() {
    $("#room")[0].hidden = true;
    $("#chat_list")[0].hidden = false;
}

function room__redraw_title(title) {
    $("#this_room_name")[0].innerText = title;
    $("#this_room_name")[0].setAttribute("data-placement", "bottom");
    $("#this_room_name")[0].setAttribute("title", title);
}

async function sc(new_cid) {
    $("#room")[0].hidden = false;
    $("#chat_list")[0].hidden = true;
    room__redraw_title(chats[new_cid]);
    last_id[new_cid] = undefined;
    document.getElementById("msg").disabled = true;
    document.getElementById("button-send").disabled = true;
    await get_key(new_cid, "current");
    if (load_messages(new_cid) == false) return;
    thisUi["opened__room_id"] = new_cid;
    $("#" + rooms_html[thisUi["opened__room_id"]])[0].style.fontWeight =
        "normal";
    document.getElementById("msg").disabled = false;
    document.getElementById("button-send").disabled = false;
}

function sendm() {
    msg = $("#msg").val().trim();
    $("#msg").val("");
    send_message(thisUi["opened__room_id"], msg);
}

req_id__callbacks = {}
async function onNewMessage(ws_event) {
    let e;
    let bt = await ws_event["data"].text();
    try {
        e = JSON.parse(bt);
    } catch (err) {
        console.log(err);
        console.log(bt);
        console.log(ws_event);
        return;
    }
    if (e["wskv"] > 0x03260630) {
        $("#modal__old_client").modal("show");
        $("#modal__old_client").on("shown.bs.modal", function() {
            window.location.href = "index.html?no-cache=" + String(e["wskv"]);
        });
        return;
    }
    if (e["event"] == "new_message") {
        if (e["room_id"] != thisUi["opened__room_id"]) {
            $("#" + rooms_html[e["room_id"]])[0].style.fontWeight = "bold";
        } else {
            await _add_msg(e["update"]);
            await _post__add_msg();
        }
    } else if (e["event"] == "new_title") {
        $("#" + rooms_html[e["room_id"]])[0].innerText =
            e["update"]["new_title"];
        chats[e["room_id"]] = e["update"]["new_title"];
        if (e["room_id"] == thisUi["opened__room_id"])
            room__redraw_title(e["update"]["new_title"]);
    } else if (e["event"] == "auth_accepted") {
        console.log("[I] websocket logged in!")
    } else if (e["event"] == "auth_rejected") {
        console.log("[E] websocket failed to log in.")
    } else if (
        typeof(e["req_id"]) === "number" &&
        typeof(req_id__callbacks[e["req_id"]]) === "function"
    ) {
        req_id__callbacks[e["req_id"]](e["result"]);
        delete req_id__callbacks[e["req_id"]];
    } else {
        console.clear();
        console.log(bt);
    }
}

function sign_in() {
    username = $("#si_u").val();
    password = $("#si_p").val();
    $.ajax({
        type: "POST",
        url: API + "/users/sign_in/keys",
        data: JSON.stringify({
            "username": username
        }),
        success: function(data) {
            if (data["details"] != "Ok") {
                $("#modal__server").modal("show");
                return;
            }
            try {
                thisUser["pk"] = decode_base64(data["u_pk"]);
                thisUser["sk"] = sc_decrypt(
                    password,
                    decode_base64(data["u_esk"]),
                    false
                );
                thisServer["pk"] = decode_base64(
                    pc_decrypt(
                        thisUser["pk"], thisUser["sk"],
                        decode_base64(data["s_pk"])
                    ),
                );
            } catch (e) {
                $("#modal__login").modal("show");
                return;
            }

            let chk = "check";
            $.ajax({
                type: "POST",
                url: API + "/users/sign_in/token",
                data: JSON.stringify({
                    "username": username,
                    "checkseq": encode_base64(
                        pc_encrypt(thisServer["pk"], chk)
                    )
                }),
                async: true,
                success: async function(data) {
                    if (data["details"] != "Ok") {
                        $("#modal__server").modal("show");
                        return;
                    }
                    try {
                        if (sodium.to_string(
                                pc_decrypt(
                                    thisUser["pk"], thisUser["sk"],
                                    decode_base64(data["d"])
                                )
                            ) != chk) {
                            $("#modal__server").modal("show");
                            return;
                        }
                    } catch (e) {
                        $("#modal__login").modal("show");
                        return;
                    }
                    thisUser["username"] = username;
                    $.ajax({
                        type: "GET",
                        async: true,
                        url: API + "/users/u/" + username,
                        success: function(data) {
                            if (data["details"] == "Ok") {
                                thisUser["user_id"] =
                                    data["result"]["user_id"];
                            }
                        }
                    });
                    thisUser["token"] = sodium.to_string(
                        pc_decrypt(
                            thisUser["pk"],
                            thisUser["sk"],
                            decode_base64(data["token"])
                        )
                    );

                    async function sonc() {
                        console.log("[I] websocket disconnected");
                        await soc();
                        /*$("#modal__server").modal("show");*/
                    };

                    async function sonp() {
                        console.log("[I] websocket connected");
                        socket.send(JSON.stringify({
                            "type": "auth",
                            "token": thisUser["token"]
                        }));
                    }

                    async function soc() {
                        console.log("[I] websocket connecting...");
                        socket = new WebSocket(WSK);
                        socket.onmessage = onNewMessage;
                        socket.onopen = sonp;
                        socket.onclose = sonc;
                    }

                    await soc();

                    list_chats();
                    $("#gui__sign_in")[0].hidden = true;
                    $("#gui__chats")[0].hidden = false;
                }
            });
        }
    }).fail(function() {
        $("#modal__server").modal("show");
        return;
    });
}

async function send_message(chat_id, data) {
    if (data == "") return;
    k = await get_key(chat_id);
    d_o = {
        "room_id": chat_id,
        "key_id": k["key_id"],
        "m_data": encode_base64(
            sc_encrypt(k["key"], data)
        )
    }
    txt = data;
    if (socket.readyState !== WebSocket.OPEN)
        $.ajax({
            type: "POST",
            url: API + "/room/m/send",
            headers: { "X-JWT": thisUser["token"] },
            data: JSON.stringify(d_o),
            async: true,
            success: function(data) {
                if (data["details"] != "Ok") {
                    $("#modal__server").modal("show");
                    window.location.reload();
                }
            }
        });
    else
        socket.send(JSON.stringify({
            "type": "room/send",
            "token": thisUser["token"],
            "room_id": chat_id,
            "key_id": k["key_id"],
            "data": encode_base64(
                sc_encrypt(k["key"], data)
            )
        }));
}



function room__edit_title(room_id, new_title) {
    $.ajax({
        async: true,
        url: API + "/room/rename",
        headers: {
            "X-JWT": thisUser["token"]
        },
        data: JSON.stringify({
            "room_id": room_id,
            "new_title": new_title
        }),
        type: "POST"
    }).then(function(api_response) {
        if (api_response["details"] != "Ok") {
            alert("Failed to rename room.");
            return;
        }
        chat = api_response["result"];
        chats[room_id] = chat["room_metadata"]["chat_name"];
        if (room_id == thisUi["opened__room_id"])
            room__redraw_title(chats[thisUi["opened__room_id"]]);
        list_chats();
    });
}


async function _http__room_k(room_id, key_id) {
    return $.ajax({
        type: "POST",
        url: API + "/room/k/" + key_id,
        headers: {
            "X-JWT": thisUser["token"]
        },
        data: JSON.stringify({
            "room_id": room_id,
        }),
        async: true
    });
}

/*
    caches all requested keys for each room
    { chat_id: { key_id: key, }, }
*/
keys_cache = {}

/* 
    help to fast resolution of current key_id for each room
    { chat_id: key_id, }
*/
keys_current = {}

async function get_key(chat_id, key_id = "current") {
    if (typeof keys_cache[chat_id] === "undefined")
        keys_cache[chat_id] = {}
    if (key_id == "current" && typeof keys_current[chat_id] !== "undefined")
        key_id = keys_current[chat_id]
    if (typeof keys_cache[chat_id][key_id] !== "undefined") {
        return {
            "key": keys_cache[chat_id][key_id],
            "key_id": key_id
        }
    }

    r = await _http__room_k(chat_id, key_id)
    k_id = r["result"]["key_id"];
    key = pc_decrypt(
        thisUser["pk"], thisUser["sk"],
        decode_base64(r["result"]["key"])
    );
    if (key_id == "current")
        keys_current[chat_id] = k_id;
    keys_cache[chat_id][k_id] = key;
    return {
        "key": key,
        "key_id": k_id,
    };
}

function _compose_bubble(sender, l = false, data, crit = false) {
    var div_html = document.createElement("div");
    ["p-2", "m-0"].forEach(
        function(c) {
            div_html.classList.add(c);
        }
    );

    var a_html = document.createElement("a");
    a_html.textContent = data;

    var p = document.createElement("div");
    ["mb-0", "small", "bold"].forEach(function(c) {
        p.classList.add(c);
    });

    p_classes = []
    if (l === false) {
        div_html.classList.add("balon");
        if (!crit) div_html.classList.add("r");
        a_html.classList.add("float-right");
        p_classes = ["text-dark", "r"];
    } else {
        div_html.classList.add("balon");
        if (!crit) div_html.classList.add("l");
        a_html.classList.add("float-left");
        p_classes = ["text-white", "l"];
    }

    if (crit)
        ["balon", "service", "crit"].forEach(
            function(e) {
                div_html.classList.add(e);
            }
        );

    var icn = document.createElement("i");
    icn.classList.add("text-small");
    icn.setAttribute("data-feather", "user");
    span = document.createElement("span");
    span.textContent = sender;
    if (l) p.append(icn);
    p.append(span);
    if (!l) p.append(icn);
    p_classes.forEach(
        function(e) {
            p.classList.add(e);
        }
    );

    div_html.id = uuid4();
    a_html.prepend(p);
    div_html.appendChild(a_html);

    return div_html;
}

let last_id = {}
let sem = false;

async function _add_msg(msg, offset = 0) {
    var nick = msg["nick"];
    var l = (nick !== thisUser["username"]);
    var ck = await get_key(thisUi["opened__room_id"], msg["key_id"]);
    var data = sodium.to_string(sc_decrypt(
        ck["key"],
        decode_base64(msg["data"])
    )).trim();
    if (data.length > 0) {
        if (offset == 0) {
            $("#msg_list").append(
                _compose_bubble(nick, l, data)
            );
        } else {
            $("#msg_list").prepend(
                _compose_bubble(nick, l, data)
            );
        }
        return true;
    }
    return false;
}

async function _post__add_msg(offset = 0) {
    feather.replace();
    ml = $("#msg_list");
    if (offset == 0) ml.scrollTop(ml[0].scrollHeight);
}

async function _http__msg_list(d_o, token, reload) {
    if (sem) return;
    let offset = d_o["offset"] || 0;
    sem = !sem;
    let added_messages = 0;
    let total_messages = 0;
    $.ajax({
        type: "POST",
        url: API + "/room/m/list",
        headers: { "X-JWT": thisUser["token"] },
        data: JSON.stringify(d_o),
        async: true,
        success: function(data) {
            if (data["details"] != "Ok") return;
            r = data["result"];

            if (!reload && offset > 0 && r.length == 0)
                last_id[d_o["room_id"]] = thisUi["opened__room_first_message"];

            if (!reload && offset == 0)
                $("#msg_list").empty();

            if (!reload && r.length > 0)
                thisUi["opened__room_first_message"] = r[0]["id"];

            if (offset > 0) r.reverse();
            added_messages = 0;
            total_messages = r.length;
            r.forEach(async function(msg) {
                try {
                    if (await _add_msg(msg, offset))
                        added_messages += 1;
                } finally {
                    await _post__add_msg(offset);
                    if (added_messages != total_messages) {
                        d_o["limit"] = total_messages - added_messages;
                        d_o["offset"] = thisUi["opened__room_first_message"];
                        _http__msg_list(d_o, token, false);
                    }
                }
            });
        },
        complete: function(e) {
            sem = !sem;
        }
    });
}

async function _ws__msg_list(room_id, token, reload, offset = 0, limit = 10) {
    req_id = Number(String(new Date().getTime()) + String(offset));
    req_id__callbacks[req_id] = async function(r) {
        if (!reload && offset > 0 && r.length == 0)
            last_id[room_id] = thisUi["opened__room_first_message"];

        if (!reload && offset == 0)
            $("#msg_list").empty();

        if (!reload && r.length > 0)
            thisUi["opened__room_first_message"] = r[0]["id"];

        if (offset > 0) r.reverse();
        added_messages = 0;
        total_messages = r.length;
        r.forEach(async function(msg) {
            try {
                if (await _add_msg(msg, offset))
                    added_messages += 1;
            } finally {
                await _post__add_msg(offset);
                if (added_messages != total_messages) {
                    limit = total_messages - added_messages;
                    offset = thisUi["opened__room_first_message"];
                    await _ws__msg_list(room_id, token, false, offset, limit);
                }
            }
        });
    };
    socket.send(JSON.stringify({
        "type": "room/load",
        "req_id": req_id,
        "room_id": room_id,
        "token": token,
        "offset": offset,
        "limit": limit
    }));
}

function load_messages(room_id, reload = false, offset = 0) {
    if (!reload && offset == 0) {
        document.getElementById("msg_list").innerText =
            "Loading messages...";
    }
    if (room_id == undefined || room_id == null) {
        document.getElementById("msg_list").innerText =
            "Invalid chat selsected!";
        return false;
    }

    post_data = {
        "room_id": room_id,
        "offset": offset
    }
    if (reload) post_data["limit"] = 1;
    if (socket.readyState === WebSocket.OPEN) {
        _ws__msg_list(room_id, thisUser["token"], reload, offset);
    } else {
        _http__msg_list(
            post_data,
            thisUser["token"],
            reload,
            offset
        );
    }
}

let chats = {}

function list_chats() {
    $.ajax({
        type: "GET",
        url: API + "/rooms/list",
        headers: { "X-JWT": thisUser["token"] },
        async: true,
        success: function(data) {
            if (data["details"] != "Ok") {
                console.log("[E] failed to /rooms/list");
                return;
            }
            $("#chat_list").empty()
            cl = data["result"];
            cl.forEach(function(chat) {
                try {
                    var div = document.createElement("div");
                    var a = document.createElement("span");
                    var id = uuid4();
                    a.id = id;
                    a.classList.add("mr-2");
                    a.classList.add("chat_name");
                    div.setAttribute(
                        "onclick",
                        "javascript:sc(" + chat["rmid"] + ");"
                    );
                    var icn = document.createElement("i");
                    icn.classList.add("text-small");
                    icn.setAttribute("data-feather", "hash");
                    div.append(icn);
                    div.append(a);
                    div_cl = [
                        "border", "border-gray", "bg-light", "mt-1", "pb-1",
                        "chat_entry"
                    ];
                    div_cl.forEach(function(c) {
                        div.classList.add(c);
                    });
                    a.textContent = JSON.parse(chat["info"])["chat_name"];
                    $("#chat_list").append(div);
                    rooms_html[chat["rmid"]] = id;
                    chats[chat["rmid"]] =
                        JSON.parse(chat["info"])["chat_name"];
                } catch (e) {
                    console.log(e);
                }
            });
            feather.replace();
        }
    });
}

let ticking = false;
let load_new = false;

function scroller(scroll_pos) {
    if (typeof last_id[thisUi["opened__room_id"]] === typeof 1)
        return;

    y = $("#msg_list")[0].scrollTop;

    if (scroll_pos == 0 && !load_new)
        $("#msg_list").scrollTop(0.05 * y);
    else if (scroll_pos == 0 && load_new) {
        $("#msg_list").scrollTop(0.025 * y);
        load_messages(thisUi["opened__room_id"], false, thisUi["opened__room_first_message"]);
    }
    load_new = !load_new;
}

$("#msg_list").on("scroll", async function(e) {
    if (!ticking && typeof thisUi["opened__room_id"] === typeof 1) {
        window.requestAnimationFrame(async function() {
            scroller($("#msg_list")[0].scrollTop);
            ticking = false;
        });

        ticking = true;
    }
});

$("#btn__room_menu").on("click", function() {
    $("#room_menu")[0].hidden = !$("#room_menu")[0].hidden;
});

$("#btn__room_rename").on("click", function() {
    $("#new_title").val($("#this_room_name")[0].innerText);
    $("#modal__rename_room").modal("toggle");
});

$("#new_title").on("keyup", function(e) {
    if (e.originalEvent.keyCode == 13) {
        e.originalEvent.preventDefault();
        $("#modal__rename_room__save_btn").focus();
        $("#modal__rename_room__save_btn").click();
    }
});

$("#modal__rename_room__save_btn").on("click", function() {
    room__edit_title(thisUi["opened__room_id"], $("#new_title").val());
    $("#new_title").val("");
    $("#modal__rename_room").modal("toggle");
});